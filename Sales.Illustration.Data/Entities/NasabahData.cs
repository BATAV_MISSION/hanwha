﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class NasabahData
    {
        [Key]
        public long NasabahDataId { get; set; }
        public string TransCode { get; set; }
        public string PolicyHolderName { get; set; }
        public Nullable<System.DateTime> PolicyHolderDOB { get; set; }
        public Nullable<bool> PolicyHolderGender { get; set; }
        public Nullable<int> PolicyHolderRiskClassId { get; set; }
        public Nullable<int> PolicyHolderAge { get; set; }
        public Nullable<bool> IsMainInsured { get; set; }
        public string InsuredName { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<bool> InsuredGender { get; set; }
        public Nullable<int> InsuredRiskClassId { get; set; }
        public Nullable<int> InsuredAge { get; set; }
        public Nullable<int> PolicyHolderRelationship { get; set; }
        public string ProductCode { get; set; }

        public virtual TransLog TransLog { get; set; }
    }
}
