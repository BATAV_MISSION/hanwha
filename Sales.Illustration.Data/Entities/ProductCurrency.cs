﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class ProductCurrency
    {
        [Key]
        public int ProductCurrId { get; set; }
        public string ProductCode { get; set; }
        public string CurrCode { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual Product Product { get; set; }
    }
}
