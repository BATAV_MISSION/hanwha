﻿using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class SummaryViewModel
    {
        public NasabahDataViewModel Nasabah { get; set; }
        public PremiumAndInvestmentViewModel Premi { get; set; }
        public RiderViewModel Rider { get; set; }
        public TopUpWithdrawalViewModel TopUp { get; set; }
        public AdditionalInsuredViewModel Additional { get; set; }
        public InsuranceCostAndRatioViewModel Insurance { get; set; }

        public string StrCurrency { get { return (Premi.MataUang == "IDR") ? "Rp. " : "Rp. "; } }
    }
}