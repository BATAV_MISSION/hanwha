﻿using System.ComponentModel;

namespace Sales.Illustration.Web.ViewModel
{
    public class InsuranceCostAndRatioViewModel
    {
        public string BiayaAsuransiTotal { get; set; }
        public string RatioPremiBerkala { get; set; }
        public string Premi { get; set; }
        public string BiayaAsuransiTambahan { get; set; }
        public string Language { get; set; }
    }
}