USE [Hanwha_SalesIllustration]
GO
/****** Object:  Table [dbo].[AnnuityFactor]    Script Date: 03-Feb-17 12:07:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AnnuityFactor](
	[AnnuityFactorId] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[Factor] [float] NULL,
	[Rate] [float] NULL,
	[ProductType] [varchar](3) NULL,
	[ProductCategory] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AnnuityFactor] PRIMARY KEY CLUSTERED 
(
	[AnnuityFactorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 03-Feb-17 12:07:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ProductCode] [varchar](5) NOT NULL,
	[ProductName] [varchar](50) NULL,
	[ProductParent] [varchar](5) NULL,
	[ProductType] [varchar](3) NULL,
	[ProductCategory] [varchar](10) NULL,
	[PolicyHolderMinAge] [int] NULL,
	[PolicyHolderMaxAge] [int] NULL,
	[InsuredMinAge] [int] NULL,
	[InsuredMaxAge] [int] NULL,
	[CovAge] [int] NULL,
	[AdminFee] [money] NULL,
	[MinBasicPremium] [money] NULL,
	[MinRegularTopUp] [money] NULL,
	[RegularTopUpRate] [float] NULL,
	[MinSingleTopUp] [money] NULL,
	[MinWithdrawal] [money] NULL,
	[MinWithdrawalBalance] [money] NULL,
	[MinPaymentYear] [int] NULL,
	[MinPercSumInsured] [int] NULL,
	[MinAmountSumInsured] [money] NULL,
	[MinRemainingBalance] [money] NULL,
	[NegativeYearValidation] [int] NULL,
	[NegativeAgeValidation] [int] NULL,
	[IsActive] [bit] NULL,
	[HasAdditionalInsured] [bit] NULL,
	[TopUpWithdrawalStartYear] [int] NULL,
	[TopUpStartYear] [int] NULL,
	[WithdrawalStartYear] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rider]    Script Date: 03-Feb-17 12:07:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rider](
	[RiderCode] [varchar](10) NOT NULL,
	[RiderName] [varchar](80) NULL,
	[RiderDesc] [varchar](80) NULL,
	[OrderNo] [int] NULL,
	[PolicyHolderMinAge] [int] NULL,
	[PolicyHolderMaxAge] [int] NULL,
	[InsuredMinAge] [int] NULL,
	[InsuredMaxAge] [int] NULL,
	[CoverTerm] [int] NULL,
	[CoverTermWizer] [int] NULL,
	[HasSumInsured] [bit] NULL,
	[MinPercSumInsured] [float] NULL,
	[MaxPercSumInsured] [float] NULL,
	[MinAmountSumInsured] [money] NULL,
	[MaxAmountSumInsured] [money] NULL,
	[HasType] [bit] NULL,
	[Description] [text] NULL,
	[IsActive] [bit] NULL,
	[Category] [varchar](10) NULL,
	[DescriptionEN] [text] NULL,
	[AllowAdditionalInsured] [varchar](5) NULL,
	[DisableRider] [varchar](50) NULL,
 CONSTRAINT [PK_Rider_1] PRIMARY KEY CLUSTERED 
(
	[RiderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AnnuityFactor] ON 

INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (1, 5, NULL, 4.47, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (2, 6, NULL, 5.21, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (3, 7, NULL, 5.92, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (4, 8, NULL, 6.58, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (5, 9, NULL, 7.21, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (6, 10, NULL, 7.8, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (7, 11, NULL, 8.36, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (8, 12, NULL, 8.89, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (9, 13, NULL, 9.38, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (10, 14, NULL, 9.85, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (11, 15, NULL, 10.29, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (12, 16, NULL, 10.71, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (13, 17, NULL, 11.11, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (14, 18, NULL, 11.48, N'tra', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (15, 17, 6, 16.86, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (16, 18, 6, 16.81, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (17, 19, 6, 16.76, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (18, 20, 6, 16.71, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (19, 21, 6, 16.65, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (20, 22, 6, 16.59, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (21, 23, 6, 16.52, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (22, 24, 6, 16.46, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (23, 25, 6, 16.38, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (24, 26, 6, 16.31, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (25, 27, 6, 16.22, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (26, 28, 6, 16.14, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (27, 29, 6, 16.05, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (28, 30, 6, 15.95, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (29, 31, 6, 15.85, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (30, 32, 6, 15.74, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (31, 33, 6, 15.62, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (32, 34, 6, 15.5, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (33, 35, 6, 15.37, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (34, 36, 6, 15.23, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (35, 37, 6, 15.08, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (36, 38, 6, 14.93, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (37, 39, 6, 14.76, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (38, 40, 6, 14.59, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (39, 41, 6, 14.41, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (40, 42, 6, 14.21, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (41, 43, 6, 14, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (42, 44, 6, 13.78, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (43, 45, 6, 13.55, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (44, 46, 6, 13.3, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (45, 47, 6, 13.04, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (46, 48, 6, 12.76, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (47, 49, 6, 12.47, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (48, 50, 6, 12.16, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (49, 51, 6, 11.83, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (50, 52, 6, 11.48, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (51, 53, 6, 11.11, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (52, 54, 6, 10.71, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (53, 55, 6, 10.29, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (54, 56, 6, 9.85, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (55, 57, 6, 9.38, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (56, 58, 6, 8.89, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (57, 59, 6, 8.36, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (58, 60, 6, 7.8, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (59, 61, 6, 7.21, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (60, 62, 6, 6.58, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (61, 63, 6, 5.92, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (62, 64, 6, 5.21, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (63, 65, 6, 4.47, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (64, 66, 6, 3.67, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (65, 67, 6, 2.83, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (66, 68, 6, 1.94, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (67, 69, 6, 1, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (68, 17, 7, 14.86, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (69, 18, 7, 14.83, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (70, 19, 7, 14.8, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (71, 20, 7, 14.77, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (72, 21, 7, 14.73, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (73, 22, 7, 14.69, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (74, 23, 7, 14.65, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (75, 24, 7, 14.61, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (76, 25, 7, 14.56, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (77, 26, 7, 14.51, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (78, 27, 7, 14.45, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (79, 28, 7, 14.39, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (80, 29, 7, 14.33, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (81, 30, 7, 14.26, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (82, 31, 7, 14.19, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (83, 32, 7, 14.12, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (84, 33, 7, 14.04, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (85, 34, 7, 13.95, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (86, 35, 7, 13.85, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (87, 36, 7, 13.75, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (88, 37, 7, 13.65, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (89, 38, 7, 13.53, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (90, 39, 7, 13.41, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (91, 40, 7, 13.28, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (92, 41, 7, 13.14, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (93, 42, 7, 12.99, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (94, 43, 7, 12.83, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (95, 44, 7, 12.65, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (96, 45, 7, 12.47, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (97, 46, 7, 12.27, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (98, 47, 7, 12.06, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (99, 48, 7, 11.84, N'ul', NULL, 1)
GO
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (100, 49, 7, 11.59, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (101, 50, 7, 11.34, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (102, 51, 7, 11.06, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (103, 52, 7, 10.76, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (104, 53, 7, 10.45, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (105, 54, 7, 10.11, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (106, 55, 7, 9.75, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (107, 56, 7, 9.36, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (108, 57, 7, 8.94, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (109, 58, 7, 8.5, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (110, 59, 7, 8.02, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (111, 60, 7, 7.52, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (112, 61, 7, 6.97, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (113, 62, 7, 6.39, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (114, 63, 7, 5.77, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (115, 64, 7, 5.1, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (116, 65, 7, 4.39, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (117, 66, 7, 3.62, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (118, 67, 7, 2.81, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (119, 68, 7, 1.93, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (120, 69, 7, 1, N'ul', NULL, 1)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (121, 18, 6, 16.52, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (122, 19, 6, 16.46, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (123, 20, 6, 16.38, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (124, 21, 6, 16.31, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (125, 22, 6, 16.22, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (126, 23, 6, 16.14, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (127, 24, 6, 16.05, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (128, 25, 6, 15.95, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (129, 26, 6, 15.85, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (130, 27, 6, 15.74, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (131, 28, 6, 15.62, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (132, 29, 6, 15.5, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (133, 30, 6, 15.37, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (134, 31, 6, 15.23, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (135, 32, 6, 15.08, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (136, 33, 6, 14.93, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (137, 34, 6, 14.76, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (138, 35, 6, 14.59, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (139, 36, 6, 14.41, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (140, 37, 6, 14.21, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (141, 38, 6, 14, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (142, 39, 6, 13.78, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (143, 40, 6, 13.55, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (144, 41, 6, 13.3, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (145, 42, 6, 13.04, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (146, 43, 6, 12.76, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (147, 44, 6, 12.47, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (148, 45, 6, 12.16, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (149, 46, 6, 11.83, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (150, 47, 6, 11.48, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (151, 48, 6, 11.11, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (152, 49, 6, 10.71, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (153, 50, 6, 10.29, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (154, 51, 6, 9.85, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (155, 52, 6, 9.38, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (156, 53, 6, 8.89, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (157, 54, 6, 8.36, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (158, 55, 6, 7.8, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (159, 56, 6, 7.21, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (160, 57, 6, 6.58, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (161, 58, 6, 5.92, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (162, 59, 6, 5.21, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (163, 60, 6, 4.47, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (164, 61, 6, 3.67, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (165, 62, 6, 2.83, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (166, 63, 6, 1.94, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (167, 64, 6, 1, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (168, 18, 7, 14.65, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (169, 19, 7, 14.61, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (170, 20, 7, 14.56, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (171, 21, 7, 14.51, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (172, 22, 7, 14.45, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (173, 23, 7, 14.39, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (174, 24, 7, 14.33, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (175, 25, 7, 14.26, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (176, 26, 7, 14.19, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (177, 27, 7, 14.12, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (178, 28, 7, 14.04, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (179, 29, 7, 13.95, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (180, 30, 7, 13.85, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (181, 31, 7, 13.75, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (182, 32, 7, 13.65, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (183, 33, 7, 13.53, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (184, 34, 7, 13.41, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (185, 35, 7, 13.28, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (186, 36, 7, 13.14, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (187, 37, 7, 12.99, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (188, 38, 7, 12.83, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (189, 39, 7, 12.65, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (190, 40, 7, 12.47, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (191, 41, 7, 12.27, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (192, 42, 7, 12.06, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (193, 43, 7, 11.84, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (194, 44, 7, 11.59, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (195, 45, 7, 11.34, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (196, 46, 7, 11.06, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (197, 47, 7, 10.76, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (198, 48, 7, 10.45, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (199, 49, 7, 10.11, N'wiz', NULL, NULL)
GO
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (200, 50, 7, 9.75, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (201, 51, 7, 9.36, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (202, 52, 7, 8.94, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (203, 53, 7, 8.5, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (204, 54, 7, 8.02, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (205, 55, 7, 7.52, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (206, 56, 7, 6.97, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (207, 57, 7, 6.39, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (208, 58, 7, 5.77, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (209, 59, 7, 5.1, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (210, 60, 7, 4.39, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (211, 61, 7, 3.62, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (212, 62, 7, 2.81, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (213, 63, 7, 1.93, N'wiz', NULL, NULL)
INSERT [dbo].[AnnuityFactor] ([AnnuityFactorId], [Year], [Factor], [Rate], [ProductType], [ProductCategory], [IsActive]) VALUES (214, 64, 7, 1, N'wiz', NULL, NULL)
SET IDENTITY_INSERT [dbo].[AnnuityFactor] OFF
INSERT [dbo].[Product] ([ProductCode], [ProductName], [ProductParent], [ProductType], [ProductCategory], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CovAge], [AdminFee], [MinBasicPremium], [MinRegularTopUp], [RegularTopUpRate], [MinSingleTopUp], [MinWithdrawal], [MinWithdrawalBalance], [MinPaymentYear], [MinPercSumInsured], [MinAmountSumInsured], [MinRemainingBalance], [NegativeYearValidation], [NegativeAgeValidation], [IsActive], [HasAdditionalInsured], [TopUpWithdrawalStartYear], [TopUpStartYear], [WithdrawalStartYear]) VALUES (N'HLEDU', N'Hanwha Education', NULL, N'tra', N'education', NULL, NULL, 216, 780, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000000.0000, NULL, NULL, NULL, 1, 0, 1, 1, 1)
INSERT [dbo].[Product] ([ProductCode], [ProductName], [ProductParent], [ProductType], [ProductCategory], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CovAge], [AdminFee], [MinBasicPremium], [MinRegularTopUp], [RegularTopUpRate], [MinSingleTopUp], [MinWithdrawal], [MinWithdrawalBalance], [MinPaymentYear], [MinPercSumInsured], [MinAmountSumInsured], [MinRemainingBalance], [NegativeYearValidation], [NegativeAgeValidation], [IsActive], [HasAdditionalInsured], [TopUpWithdrawalStartYear], [TopUpStartYear], [WithdrawalStartYear]) VALUES (N'HLREG', N'Hanwha Link Reguler', NULL, N'ul', N'regular', 204, 900, 1, 828, 1188, 30000.0000, NULL, NULL, 95, 1000000.0000, 1000000.0000, 5000000.0000, 36, 500, 7500000.0000, 5000000.0000, 10, 65, 1, 1, 1, 1, 1)
INSERT [dbo].[Product] ([ProductCode], [ProductName], [ProductParent], [ProductType], [ProductCategory], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CovAge], [AdminFee], [MinBasicPremium], [MinRegularTopUp], [RegularTopUpRate], [MinSingleTopUp], [MinWithdrawal], [MinWithdrawalBalance], [MinPaymentYear], [MinPercSumInsured], [MinAmountSumInsured], [MinRemainingBalance], [NegativeYearValidation], [NegativeAgeValidation], [IsActive], [HasAdditionalInsured], [TopUpWithdrawalStartYear], [TopUpStartYear], [WithdrawalStartYear]) VALUES (N'HLSIN', N'Hanwha Link Single', NULL, N'ul', N'single', 204, 900, 1, 828, 1188, 30000.0000, 10000000.0000, 1000000.0000, 95, 1000000.0000, 1000000.0000, 5000000.0000, NULL, 125, 15000000.0000, 5000000.0000, 10, 65, 1, 0, 2, 2, 1)
INSERT [dbo].[Product] ([ProductCode], [ProductName], [ProductParent], [ProductType], [ProductCategory], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CovAge], [AdminFee], [MinBasicPremium], [MinRegularTopUp], [RegularTopUpRate], [MinSingleTopUp], [MinWithdrawal], [MinWithdrawalBalance], [MinPaymentYear], [MinPercSumInsured], [MinAmountSumInsured], [MinRemainingBalance], [NegativeYearValidation], [NegativeAgeValidation], [IsActive], [HasAdditionalInsured], [TopUpWithdrawalStartYear], [TopUpStartYear], [WithdrawalStartYear]) VALUES (N'HLWIZ', N'Hanwha Link Wizer', NULL, N'ul', N'wizer', 216, 840, 204, 840, 1188, 30000.0000, NULL, 400.0000, 100, 1000000.0000, 1000000.0000, 5000000.0000, 36, 500, 7500000.0000, 5000000.0000, 10, 65, 1, 0, 1, 1, 1)
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'ADB', N'ADB', N'Accident Death Benefit', 1, 204, 900, 1, 828, 840, 780, 1, 100, 300, 7500000.0000, 5000000000.0000, 0, N'Apabila Tertanggung meninggal dunia karena kecelakaan sebelum Tertanggung mencapai usia 70 tahun, Penanggung akan memberikan manfaat asuransi sebesar 100% Uang Pertanggungan ADB', 1, N'Risk', N'If the insured dies due to an accident before the insured reaches the age of 70 years, the Insurer will provide the insurance benefits of 100% sum assured ADB', N'YES', N'ADDB')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'ADDB', N'ADDB', N'Accident Death & Dismemberment Benefit', 2, 204, 900, 1, 828, 840, NULL, 1, 100, 300, 7500000.0000, 5000000000.0000, 0, N'Apabila Tertanggung meninggal dunia atau mengalami Cacat Tetap sebagian atau Cacat Tetap Total karena kecelakaan sebelum Tertanggung mencapai usia 70 tahun, Penanggung akan memberikan manfaat asuransi 100% Uang Pertanggungan ADDB', 1, N'Risk', N'If the insured died or suffered partial permanent disability or total permanent disability due to accident before the insured reaches the age of 70 years, the Insurer will provide insurance benefits 100% of Sum Assured ADDB', N'YES', N'TPD;ADB')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'AHCP', N'AHCP', N'Accident Hospital Cash Plan', 5, 204, 900, 1, 780, 840, NULL, 0, NULL, NULL, NULL, NULL, 1, N'Apabila Tertanggung dirawat Inap di Rumah Sakit akibat kecelakaan sebelum berusia 65 tahun, Penanggung akan membayarkan manfaat asuransi sesuai dengan plan yang dipilih yang tercantum pada Tabel Manfaat AHCP', 1, N'Unit', N'If the Insured was treated at the Hospital Confinement due  accident before 65 years old, the Insurer will pay insurance benefits according to the plan selected are listed in Table Benefit of AHCP', N'YES', N'HCP')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'CI', N'CI', N'Critical Illness', 10, 204, 900, 1, 828, 840, NULL, 1, 25, 300, NULL, 5000000000.0000, 0, N'Apabila Tertanggung didiagnosa pertama kali salah satu dari 52 penyakit kritis sebelum Tertanggung mencapai usia 70 tahun, Penanggung akan memberikan manfaat 100% Uang Pertanggungan CI tanpa mengurangi manfaat Uang Pertanggungan Asuransi Dasar', 1, N'Basic', N'If the Insured was diagnosed the first time one of the 52 critical illnesses before the insured reaches the age of 70 years, the Insurer will provide the benefits of 100% sum assured CI without reducing the benefits of the Basic Sum Assured Insurance', N'YES', N'CIACC')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'CIACC', N'CI Accelerated', N'Critical Illness Accelerated', 11, 204, 900, 1, 828, 840, NULL, NULL, 50, 50, NULL, 5000000000.0000, 0, N'Apabila Tertanggung didiagnosa pertama kali salah satu dari 52 penyakit kritis sebelum Tertanggung mencapai usia 70 tahun, Penanggung akan memberikan manfaat 100% Uang Pertanggungan CI dengan manfaat akselerasi Uang Pertanggungan Asuransi Dasar', 1, N'Basic', N'If the Insured was diagnosed the first time one of the 52 critical illnesses before the insured reaches the age of 70 years, the Insurer will provide the benefits of 100% sum assured CI with the acceleration benefits of Basic Sum Assured Insurance', N'YES', N'CI')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'HCP', N'HCP', N'Hospital Cash Plan', 4, 204, 900, 1, 780, 840, NULL, 0, NULL, NULL, NULL, NULL, 1, N'Apabila Tertanggung dirawat Inap di Rumah Sakit akibat sakit atau kecelakaan sebelum berusia 65 tahun, Penanggung akan membayarkan manfaat asuransi sesuai dengan plan yang dipilih yang tercantum pada Tabel Manfaat HCP', NULL, N'Unit', N'If the Insured was treated at the Hospital Confinement due to illness or accident before 65 years old, the Insurer will pay insurance benefits according to the plan selected are listed in Table Benefit of HCP', N'YES', N'AHCP')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'MG', N'Medi Guard', N'Medi Guard', 6, 204, 900, 1, 780, 840, NULL, 0, NULL, NULL, NULL, NULL, 1, N'Apabila Tertanggung dirawat Inap di Rumah Sakit akibat sakit atau kecelakaan sebelum berusia 65 tahun, Penanggung akan membayarkan manfaat asuransi sesuai dengan plan yang dipilih yang tercantum pada Tabel Manfaat Medi Guard', 1, N'Choice', N'If the Insured was treated at the Hospital Confinement due to illness or accident before 65 years old, the Insurer will pay insurance benefits according to the plan selected are listed in Table Benefit of Medi Guard', N'NO', NULL)
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'POP', N'PoP', N'Payor of Premium', 9, 204, 828, 204, 828, 840, NULL, NULL, NULL, NULL, NULL, 1500000000.0000, 1, NULL, 1, N'Choices', NULL, N'NO', N'SPOP;WOP')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'SP', N'SP', N'Spouse Payor of Premium', 8, 204, 900, 204, 828, 840, NULL, NULL, NULL, NULL, NULL, 1500000000.0000, 1, NULL, 1, N'Choices', NULL, N'2', N'WOP;POP')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'TPD', N'TPD', N'Total Permanent Disability', 3, 204, 900, 1, 780, 840, NULL, 1, 100, 300, NULL, NULL, 0, N'Apabila Tertanggung mengalami Cacat Tetap Total karena sakit atau karena kecelakaan sebelum Tertanggung mencapai usia 65 tahun, Penanggung akan memberikan manfaat asuransi 100% Uang Pertanggungan TPD tanpa mengurangi manfaat Uang Pertanggungan Asuransi Dasar', 1, N'Basic', N'If the Insured suffered Total Permanent Disability due to illness or an accident before the insured reaches the age of 65, the Insurer will provide insurance benefits 100% of Sum Assured TPD without reducing the benefits of the Basic Sum Assured Insurance', N'NO', N'ADDB')
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'TR', N'Term Rider', N'Term Rider', 12, 204, 900, 1, 780, 840, NULL, 1, 100, 300, NULL, 5000000000.0000, 0, N'Apabila Tertanggung meninggal dunia karena sakit atau karena kecelakaan sebelum Tertanggung mencapai usia 65 tahun, Penanggung akan memberikan manfaat 100% Uang Pertanggungan Term Rider', 1, N'Basic', N'If the insured dies due to illness or an accident before the insured reaches the age of 65, the Insurer will provide the insurance benefits 100% of Sum Assured Term Rider', N'YES', NULL)
INSERT [dbo].[Rider] ([RiderCode], [RiderName], [RiderDesc], [OrderNo], [PolicyHolderMinAge], [PolicyHolderMaxAge], [InsuredMinAge], [InsuredMaxAge], [CoverTerm], [CoverTermWizer], [HasSumInsured], [MinPercSumInsured], [MaxPercSumInsured], [MinAmountSumInsured], [MaxAmountSumInsured], [HasType], [Description], [IsActive], [Category], [DescriptionEN], [AllowAdditionalInsured], [DisableRider]) VALUES (N'WOP', N'WoP', N'Waiver of Premium', 7, NULL, NULL, 204, 828, 840, 780, 0, NULL, NULL, NULL, NULL, 1, NULL, 1, N'Choices', NULL, N'NO', N'SPOP;POP')
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3 digit : reg for reguler, tra for traditional, sin for single' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductType'
GO
